function calculateAverage() {
    var input = document.getElementById("sensor-input").value;
    var measurements = input.split("@");
    
    var sensorMap = {};
    
    // Обработка показаний
    for (var i = 0; i < measurements.length; i++) {
    var parts = measurements[i].trim().split(" ");
    var sensorId = parts[0];
    var temperature = parseFloat(parts[1]);
    
    if (sensorMap[sensorId]) {
    sensorMap[sensorId].count++;
    sensorMap[sensorId].total += temperature;
    } else {
    sensorMap[sensorId] = {
    count: 1,
    total: temperature
    };
    }
    }
    
    // Вывод результатов
    var resultTable = document.getElementById("result-table");
    resultTable.innerHTML = "";
    
    for (var sensorId in sensorMap) {
    var average = (sensorMap[sensorId].total / sensorMap[sensorId].count).toFixed(1);
    
    var row = document.createElement("tr");
    var idCell = document.createElement("td");
    var averageCell = document.createElement("td");
    
    idCell.innerText = sensorId;
    averageCell.innerText = average;
    
    row.append(idCell);
    row.append(averageCell);
    resultTable.append(row);
    }
}
