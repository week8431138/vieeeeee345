function calculateEntropy() {
    var inputText = document.getElementById("inputText").value;
    var result = entropy(inputText);
    document.getElementById("outputResult").innerHTML = "Энтропия: " + result.toFixed(2);
    }
    
    function entropy(text) {
    var characterCounts = {};
    var entropy = 0;
    
    for (var i = 0; i < text.length; i++) {
    var character = text.charAt(i);
    if (characterCounts[character]) {
    characterCounts[character]++;
    } else {
    characterCounts[character] = 1;
    }
    }
    
    for (var character in characterCounts) {
    var frequency = characterCounts[character] / text.length;
    entropy -= frequency * Math.log2(frequency);
    }
    
    return entropy;
    }
