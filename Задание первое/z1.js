function searchOccurrences() {
    // Получение значений из формы
    var searchText = document.getElementById("searchText").value;
    var inputText = document.getElementById("inputText").value;
    var windowSize = parseInt(document.getElementById("windowSize").value);
    
    // Поиск вхождений строки
    var occurrences = [];
    for (var i = 0; i <= inputText.length - searchText.length; i++) {
    var window = inputText.slice(i, i + searchText.length);
    if (window === searchText) {
    occurrences.push(inputText.slice(i, i + windowSize));
    }
    }
    
    // Вывод результатов
    var outputDiv = document.getElementById("output");
    outputDiv.innerHTML = occurrences.length > 0 ? occurrences.join(", ") : "Нет вхождений строки.";
}
